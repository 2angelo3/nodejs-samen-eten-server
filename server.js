const express = require('express')
const bodyParser = require('body-parser')
const studenthomerouter = require('./src/routes/studenthome.router')
const studentmealrouter = require('./src/routes/meal.router')
const authenticationrouter = require('./src/routes/authentication.routes')
const participantsrouter = require('./src/routes/participants.router')
require('dotenv').config()
const config = require('./src/config/config')
const logger = config.logger

const app = express()

app.use(bodyParser.json())

const port = process.env.PORT || 3000

app.all('*', (req, res, next) => {
  const method = req.method
  logger.debug('Method', method)
  next()
})

app.use('/api', studenthomerouter)
app.use('/api', studentmealrouter)
app.use('/api', authenticationrouter)
app.use('/api', participantsrouter)

// calls all the wrong urls
app.all('*', (req, res, next) => {
  res.status(404).json({
    error: "Endpoint does not exist!"
  })
})

app.listen(port, () =>
  logger.info(`Example app listening at ${port}`))

  module.exports = app

  