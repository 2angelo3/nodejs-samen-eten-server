process.env.DB_DATABASE = 'studenthome'
process.env.NODE_ENV = 'testing'
process.env.LOGLEVEL = 'error'
console.log(`Running tests using database '${process.env.DB_DATABASE}'`)

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../../server')
const connection = require('../../src/config/connection')
const logger = require('../../src/config/config').logger


chai.should()
chai.use(chaiHttp)

const CLEAR_DB = 'DELETE IGNORE FROM `user`'

before((done) => {
  // console.log('beforeEach')
  connection.query(CLEAR_DB, (err, rows, fields) => {
    if (err) {
      logger.debug(`beforeEach CLEAR error: ${err}`)
      done(err)
    } else {
      done()
    }
  })
})

// After successful register we have a valid token. We export this token
// for usage in other testcases that require login.
// let validToken

describe('Manage Authentication', () => {
    //let createStudentid
    describe('UC-101 Registreren', () => {
      //Before we start delete the test data
      beforeEach((done) => {
        connection.query(`DELETE FROM user WHERE Email = "hallo@test.nl"`,
          (err, rows, fields) => {
            if (err) {
              done(err)
            } else {
              done()
            }
          }
        )
      })
      it('TC-101-1 Verplichte veld ontbreekt', (done) => {
        chai
          .request(server)
          .post('/api/register')
          .send({
            lastname: 'LastName',
            email: 'test@test.nl',
            studentnr: '1234567',
            password: 'secret'
          })
          //Send request with missing firstname
          .end((err, res) => {
            res.should.have.status(400)
            res.body.should.be.a('object')
  
            const { error } = res.body
            error.should.be
              .an('string')
              .that.equals(
                'AssertionError [ERR_ASSERTION]: firstname must be a string.'
              )
            //Firstname is missing so we send an error message to the body.
            done()
          })
      })
      it('TC-101-2 Invalid email', (done) => {
        chai
          .request(server)
          .post('/api/register')
          .send({
            firstname: 'Klaas',
            lastname: 'LastName',
            email: 'test',
            studentnr: '1234567',
            password: 'secret'
          })
          //Send request with wrong email format.
          .end((err, res) => {
            res.should.have.status(400)
            res.body.should.be.a('object')
  
            const { error } = res.body
            error.should.be
              .an('string')
              .that.equals(
                'AssertionError [ERR_ASSERTION]: wrong email'
              )
            //Email is invalid error.
            done()
          })
      })
      it('TC-101-3 Invalide wachtwoord', (done) => {
        // server starten
        // POST versturen, onvolledige info meesturen
        // Valideren dat verwachte waarden kloppen
  
        chai
          .request(server)
          .post('/api/register')
          .send({
            firstname: 'Jan',
            lastname: 'Smit',
            email: 'jsmit@server.nl',
            studentnr: '222222',
            password: 4
          })
          .end((err, res) => {
            res.should.have.status(400)
            res.body.should.be.a('object')
  
            const { error } = res.body
            error.should.be
              .an('string')
              .that.equals(
                'AssertionError [ERR_ASSERTION]: password must be a string.'
              )
            done()
          })
      })
      it('TC-101-4 Gebruiker bestaat al', (done) => {
        // server starten
        // POST versturen, onvolledige info meesturen
        // Valideren dat verwachte waarden kloppen
  
        chai
          .request(server)
          .post('/api/register')
          .send({
            firstname: 'Jan',
            lastname: 'Smit',
            email: 'jsmit@server.nl',
            student_number: 222222,
            password: 'secret'
          })
          .end((err, res) => {
            res.should.have.status(400)
            res.body.should.be.a('object')
  
            const { error } = res.body
            error.should.be
              .an('string')
              .that.equals('This user has already been taken.')
            done()
          })
      })
  
      it('TC-101-5 Gebruiker succesvol geregistreerd', (done) => {
        // server starten
        // POST versturen, onvolledige info meesturen
        // Valideren dat verwachte waarden kloppen
  
        chai
          .request(server)
          .post('/api/register')
          .send({
            firstname: 'FirstName',
            lastname: 'LastName',
            email: 'test@test.nl',
            studentnr: 1234567,
            password: 'secret'
          })
          .end((err, res) => {
            res.should.have.status(200)
            res.body.should.be.a('object')
  
            const response = res.body
            response.should.have.property('token').which.is.a('string')
            response.should.have.property('username').which.is.a('string')
  
            done()
          })
      })
    })
  })
  describe('UC-102 Login', () => {
    //Before we start delete the test data
    beforeEach((done) => {
      connection.query(
        `DELETE FROM user WHERE Email = "test@test.nl"`,
        (err, rows, fields) => {
          if (err) {
            done(err)
          } else {
            done()
          }
        }
      )
    })
    it('TC-102-1 Verplicht veld ontbreekt', (done) => {
      chai
        .request(server)
        .post('/api/login')
        .send({
          password: 'secret'
        })
        //Send request with missing firstname
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.be.a('object')
  
          const { error } = res.body
          error.should.be
            .an('string')
            .that.equals(
              'AssertionError [ERR_ASSERTION]: email must be a string.'
            )
          //Firstname is missing so we send an error message to the body.
          done()
        })
    })
    it('TC-102-2 Invalide email adres', (done) => {
      chai
        .request(server)
        .post('/api/login')
        .send({
          email: 'ha123@',
          password: 'secret'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.be.a('object')
  
          const { error } = res.body
          error.should.be
            .an('string')
            .that.equals('AssertionError [ERR_ASSERTION]: wrong email')
          done()
        })
    })
    it('TC-102-3 Invalide wachtwoord', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      chai
        .request(server)
        .post('/api/login')
        .send({
          email: 'test@test.nl',
          password: 'secresdadsa'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.be.a('object')
  
          const { error } = res.body
          error.should.be
            .an('string')
            .that.equals('User not found or password is invalid')
          done()
        })
    })
    it('TC-102-4 Gebruiker bestaat niet', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
  
      chai
        .request(server)
        .post('/api/login')
        .send({
          email: 'jsmit@servehjgjhjr.nl',
          password: 'secret'
        })
        .end((err, res) => {
          res.should.have.status(400)
          res.body.should.be.a('object')
  
          const { error } = res.body
          error.should.be
            .an('string')
            .that.equals('User not found or password is invalid')
          done()
        })
    })
    it('TC-102-5 Gebruiker succesvol ingelogd', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
  
      chai
        .request(server)
        .post('/api/login')
        .send({
          email: 'jsmit@server.nl',
          password: 'secret'
        })
        .end((err, res) => {
          res.should.have.status(200)
          res.body.should.be.a('object')
          done()
        })
    })
  })
