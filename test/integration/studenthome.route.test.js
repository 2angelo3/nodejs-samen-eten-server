process.env.DB_DATABASE = 'testing'

const chai = require('chai')
var should = require('chai').should()
const chaiHttp = require('chai-http')
const server = require('../../server')
const tracer = require('tracer')
const connection = require('../../src/config/connection')

chai.should()
chai.use(chaiHttp)
tracer.setLevel('error')
const jwt = require('jsonwebtoken')

// jwt.sign({id: 1}, process.env.KEY, {expiresIn: '2h' }, (err, token)=>{})
let usertokenID

describe('Manage studenthomes', () => {
  //let createStudentid
  let deleteid
  describe('UC-201 Create studenthome - POST api/studenthome', () => {
    beforeEach((done) => {
      connection.query(
        `DELETE FROM studenthome WHERE Name = "avans"`,
        (err, rows, fields) => {
          if (err) {
            done(err)
          } else {
            done()
          }
        }
      )
      // done()
    })

    it('TC-201-1 should return valid error when required value is not present', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      jwt.sign(
        { id: 1 },
        process.env.KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .post('/api/studenthome')
            .set('authorization', 'Bearer ' + token)
            .send({
              Name: 'Angelo',
              House_Nr: 15,
              UserID: 1,
              postcode: '2176 TT',
              telefoonnummer: '06123456222',
              City: 'Teststad'
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')
              let { message, error } = res.body

              message.should.be.an('string').that.equals('An error occured while creating a student home.')
              error.should.be
                .an('string')
                .that.equals('AssertionError [ERR_ASSERTION]: straatnaam is nodig')

              done()
            })
        }
      )
    })

    it('TC-201-2 should return a valid error when postal code is invalid. ', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .post('/api/studenthome')
            .set('authorization', 'Bearer ' + token)
            .send({
              Name: 'Angelo',
              Address: "kruisweg",
              House_Nr: 15,
              UserID: 1,
              Postal_Code: '2176',
              Telephone: '06123456222',
              City: 'Teststad'
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')
              let { message, error } = res.body
              message.should.be.an('string').that.equals('An error occured while creating a student home.')
              error.should.be
                .an('string')
                .that.equals('AssertionError [ERR_ASSERTION]: foute postcode')

              done()
            })
        }
      )
    })

    it('TC-201-3 should return a valid error when telephone number code is invalid. ', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .post('/api/studenthome')
            .set('authorization', 'Bearer ' + token)
            .send({
              homeId: '10',
              Name: 'Angelo',
              Address: 'kruisweg',
              House_Nr: 15,
              UserID: 1,
              Postal_Code: '2176 TT',
              Telephone: '06123',
              City: 'Teststad'
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')
              let { message, error } = res.body
              message.should.be.an('string').that.equals('An error occured while creating a student home.')
              error.should.be
                .an('string')
                .that.equals(
                  'AssertionError [ERR_ASSERTION]: foute telefoonnummer'
                )

              done()
            })
        }
      )
    })

    it('TC-201-4 Studentenhuis bestaat al', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      jwt.sign(
        { id: 1 },
        process.env.KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .post('/api/studenthome')
            .set('authorization', 'Bearer ' + token)
            .send({
              Name: 'Princenhage',
              Address: 'ttde',
              House_Nr: 15,
              UserID: 1,
              Postal_Code: '2176 JJ',
              Telephone: '06123456222',
              City: 'Teststad'
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')
              let { result } = res.body

              result.should.be
                .an('string')
                .that.equals('Studenthome already exists')

              done()
            })
        }
      )
    })

    it('TC-201-5 Niet ingelogd', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .post('/api/studenthome')
            .set('authorization', 'Bearer ' + 'd')
            .send({
              Name: 'claudiusprinsenwijk',
              Address: 'Adres',
              House_Nr: 4,
              UserID: 1,
              Postal_Code: '4621JE',
              Telephone: '1641418357',
              City: 'breda'
            })
            .end((err, res) => {
              res.should.have.status(401)
              res.should.be.an('object')
              const { error } = res.body
              error.should.be.an('string').that.equals('Not authorized')
              done()
            })
        }
      )
    })

    it('TC-201-6 Studentenhuis  succesvol toegevoegd ', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .post('/api/studenthome')
            .set('authorization', 'Bearer ' + token)
            .send({
              Name: 'avans',
              Address: 'ttde',
              House_Nr: 15,
              UserID: 1,
              Postal_Code: '2176 YY',
              Telephone: '06123444444',
              City: 'Teststad'

            })
            .end((err, res) => {
              res.should.have.status(200)
              res.should.be.an('object')
              let { result } = res.body
              createStudentid = result.Id
              result.result.should.be.an('object')

              done()
            })
        }
      )
    })
  })
  describe('UC-202 Overzichten studenthomes - GET api/studenthome', () => {
    beforeEach((done) => {
      done()
    })

    it('TC-202-1 Toon nul studentenhuizen', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen

      chai

        .request(server)
        .get('/api/studenthome?name=hansklok')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body
          result.result.should.be.an('array')
          result.result.length.should.equal(0)

          done()
        })
    })


    it('TC-202-2 show 2 student homes', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      chai
        .request(server)
        .get('/api/studenthome?city=amsterdam')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body

          result.result.length.should.equal(2)

          done()
        })
    })

    it('TC-202-4 Toon studentenhuizen met zoekterm op niet-bestaande naam', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      chai

        .request(server)
        .get('/api/studenthome?name=-1')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body
          result.result.should.be.an('array')
          result.result.length.should.equal(0)

          done()
        })
    })

    it('TC-202-3 Toon studentenhuizen met zoekterm op niet-bestaande stad', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      chai

        .request(server)
        .get('/api/studenthome?city=bestaatniet')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body
          result.result.should.be.an('array')
          result.result.length.should.equal(0)

          done()
        })
    })
    it('TC-202-3 TC-202-5 Toon studentenhuizen met zoekterm op bestaande stad', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      chai

        .request(server)
        .get('/api/studenthome?city=Breda')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body
          result.result.should.be.an('array')

          done()
        })
    })

    it('TC-202-6 Toon studentenhuizen met zoekterm op bestaande naam', (done) => {
      // server starten
      // POST versturen, onvolledige info meesturen
      // Valideren dat verwachte waarden kloppen
      chai

        .request(server)
        .get('/api/studenthome?name=Olegna')
        .end((err, res) => {
          res.should.have.status(200)
          res.should.be.an('object')

          let { result } = res.body
          result.result.should.be.an('array')

          done()
        })
    })
  })
  describe('UC-204 Studentenhuis wijzigen', () => {
    beforeEach((done) => {
      done()
    })
    it('TC-204-1 Verplicht veld ontbreekt', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .put('/api/studenthome/1')
            .set('authorization', 'Bearer ' + token)
            .send({
              Address: 'ttde',
              House_Nr: 15,
              UserID: 1,
              Postal_Code: '2176 JJ',
              Telephone: '06444444444',
              City: 'Teststad',
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')
              const { message, error } = res.body
              message.should.be
                .an('string')
                .that.equals('An error occured while creating a student home.')
              error.should.be.an('string')
              done()
            })
        }
      )
    })
    it('TC-204-2 Invalide postcode', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
            .request(server)
            .put('/api/studenthome/1')
            .set('authorization', 'Bearer ' + token)
            .send({
              id: 35,
              Name: 'oliebol',
              Address: 'ttde',
              House_Nr: 15,
              UserID: 1,
              Postal_Code: '2176 J',
              Telephone: '06444444444',
              City: 'Teststad',
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')
              const { message, error } = res.body
              message.should.be
                .an('string')
                .that.equals('An error occured while creating a student home.')
              error.should.be
                .an('string')
                .that.equals('AssertionError [ERR_ASSERTION]: foute postcode')
              done()
            })
        }
      )
    })
    it('TC-204-3 Invalid telefoonnummer', (done) => {
      jwt.sign(
        { id: 1 },
        process.env.KEY,
        { expiresIn: '2h' },
        (err, token) => {
          chai
          chai
            .request(server)
            .put('/api/studenthome/1')
            .set('authorization', 'Bearer ' + token)
            .send({
              id: 35,
              Name: 'oliebol',
              Address: 'ttde',
              House_Nr: 15,
              UserID: 1,
              Postal_Code: '2176 JJ',
              Telephone: '060',
              City: 'Teststad',
            })
            .end((err, res) => {
              res.should.have.status(400)
              res.should.be.an('object')
              const { message, error } = res.body
              message.should.be
                .an('string')
                .that.equals('An error occured while creating a student home.')
              error.should.be
                .an('string')
                .that.equals(
                  'AssertionError [ERR_ASSERTION]: foute telefoonnummer'
                )
              done()
            })
        }
      )
    })
    it('TC-204-4 Studentenhuis bestaat niet', (done) => {
      jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
        chai
          .request(server)
          .put('/api/studenthome/2212')
          .set('authorization', 'Bearer ' + token)
          .send({
            Name: 'hfff',
            Address: 'Adres',
            House_Nr: 4,
            UserID: 3,
            Postal_Code: '4621 JE',
            Telephone: '06744444445',
            City: 'breda'
          })
          .end((err, res) => {
            res.should.have.status(200)
            res.should.be.an('object')
            const { result } = res.body
            result.should.be
              .an('string')
              .that.equals('Studenthome does not exist')
            done()
          })
      })
    })
  
    it('TC-204-5 Niet ingelogd', (done) => {
      jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
        chai
          .request(server)
          .put('/api/studenthome/61')
          .set('authorization', 'Bearer ' + 'h')
          .send({
            Name: 'testnaamupdate',
            Address: 'Adres ',
            House_Nr: 4,
            UserID: 1,
            Postal_Code: '4621JE',
            Telephone: '1641418357',
            City: 'breda'
          })
          .end((err, res) => {
            res.should.have.status(401)
            res.should.be.an('object')
            const { error } = res.body
            error.should.be.an('string').that.equals('Not authorized')
            done()
          })
      })
    })
    it('TC-204-6 Studentenhuis  succesvol gewijzigd', (done) => {
      jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
        chai
          .request(server)
          .put('/api/studenthome/1')
          .set('authorization', 'Bearer ' + token)
          .send({
            Name: 'oliebol',
            Address: 'ttde',
            House_Nr: 15,
            UserID: 1,
            Postal_Code: '2176 JJ',
            Telephone: '06444444443',
            City: 'teststad'
          })
          .end((err, res) => {
            res.should.have.status(200)
            res.should.be.an('object')
            const { result } = res.body
            result.should.be.an('object')
            done()
          })
      })
    })
  })
  describe('UC-205 Studentenhuis verwijderen', () => {
    beforeEach((done) => {
      connection.query(
        `DELETE FROM studenthome WHERE Name = "niewehuistoeveoegen"`,
        (err, rows, fields) => {
          if (err) {
            done(err)
          } else {
            deleteid = rows
            done()
          }
        }
      )
    })
    it('TC-205-1 Studentenhuis bestaat niet', (done) => {
      jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
        chai
          .request(server)
          .delete('/api/studenthome/9999')
          .set('authorization', 'Bearer ' + token)
          .end((err, res) => {
            res.should.have.status(200)
            res.should.be.an('object')
            const { result } = res.body
            result.should.be
              .an('string')
              .that.equals('Studenthome does not exist')
            done()
          })
      })
    })
    it('TC-205-2 Niet ingelogd', (done) => {
      jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
        chai
          .request(server)
          .delete('/api/studenthome/25')
          .set('authorization', 'Bearer ' + 'h')
          .end((err, res) => {
            res.should.have.status(401)
            res.should.be.an('object')
            const { error } = res.body
            error.should.be.an('string').that.equals('Not authorized')
            done()
          })
      })
    })
    it('TC-205-3 Actor is geen eigenaar', (done) => {
      jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
        chai
          .request(server)
          .delete('/api/studenthome/10')
          .set('authorization', 'Bearer ' + 'h')
          .end((err, res) => {
            res.should.have.status(401)
            res.should.be.an('object')
            const { error } = res.body
            error.should.be.an('string').that.equals('Not authorized')
            done()
          })
      })
    })
  
    it('TC-205-4 Studentenhuis  succesvol verwijderd', (done) => {
      jwt.sign({ id: 1 }, process.env.KEY, { expiresIn: '2h' }, (err, token) => {
        chai
          .request(server)
          .delete(`/api/studenthome/29`)
          .set('authorization', 'Bearer ' + token)
          .end((err, res) => {
            res.should.have.status(200)
            res.should.be.an('object')
            const { result } = res.body
            done()
          })
      })
    })
})
})