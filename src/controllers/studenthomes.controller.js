const config = require('../config/config')
const logger = config.logger
const assert = require('assert')
const database = require('../config/database')
const pool = require('../config/database')

let controller = {
  
    getstudentinfo(req, res)  {
      logger.info("Getrequest is called on api/info!")
    
      const info = {
        Studentnaam: "Angelo van Peer",
        Studentnummer: "2139170",
        Beschrijving: "Dit is een nodejs server waarbij info word getoond wat de studenten gaan eten",
        "Sonarqube Url": ""
      }
      res.status(200).json(info)
    },
  
  validateStudenthome(req, res, next) {
    logger.info('Validating student home...')

    try {
      const { Name, Address, House_Nr, Postal_Code, City, Telephone } = req.body

      assert(typeof Name === 'string', "naam is nodig")
      assert(typeof Address === 'string', "straatnaam is nodig")
      assert(typeof House_Nr  === 'number', "huisnummer is nodig")
      assert(typeof Postal_Code === 'string', "postcode is nodig")
      assert.match(Postal_Code, /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i, "foute postcode")
      assert(typeof City === 'string', "plaats is nodig")
      assert(typeof Telephone === 'string', "telefoonnummer is nodig")
      assert.match(Telephone, /(^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^00[0-9]{2}|^0)([0-9]{9}$|[0-9\-\s]{10}$)/, "foute telefoonnummer")
    
      next()
    } catch (e) {
      res.status(400).json({
          message: 'An error occured while creating a student home.',
          error: e.toString()
      })
    
    }
  },

  createStudenthome(req, res, next) {
    const home = req.body
    database.createstudenthome(home, (err, result) => {
      if (err) {
        res.status(400).json({
          result: err
        })
      } else {
        if (result.result.code === 'ER_DUP_ENTRY') {
          res.status(400).json({
            result: 'Studenthome already exists'
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      }
    })
  },
  
  updateStudenthome(req, res, next) {

    logger.info('studenthome is updated and called on /api/studenthome/')
  
    const updatedHome = req.body
    const homeId = req.params.homeId
      database.updateStudenthomes(updatedHome, homeId, (err, result) => {

        if(err) {
          res.status(400).json({
            result: err
          })
        } else {
          res.status(200).json({
            result: result.result
          })
        }

      })
  },

    getdetailOfStudenthomes(req, res, next)  {
        logger.info('Get is called on /api/studenthome/:homeId')
      
        const homeId = req.params.homeId
        logger.info('Get is called on /api/studenthome/', homeId)
      
        database.getStudenthome(homeId, (err, result) => {

          if(err) {
        res.status(400).json({
          result: err
        })
      } else{
        res.status(200).json({
          result: result
        })
      }
    })

   }, 

    getListOfAllStudents (req, res, next)  {
    logger.info('Get is called on /api/studenthome?name=:name&city=:city')
    const name = req.query.name
    const city = req.query.city
  
  
    //name and city is empty
    if(name && city){
      logger.debug('name and city is empty')
       database.getnameAndCity(name, city, (err, result) => {
        
        if(err) {
          res.status(400).json({
            result: err
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
       })
      } 

    //name and city is undefined
    else if(name === undefined && city === undefined){
      logger.debug('name and city are both undifined ')
      database.getAllstudenthomes((err, result) => {
        
        if(err) {
          res.status(400).json({
            result: err
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
       })
      }  
    //name is empty, city is defined
    else if(name){
      logger.debug('name is empty, city is defined ')
      database.getName(name, (err, result) => {
        if(err) {
          res.status(400).json({
            result: err
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      })
    }  else if(city){
      logger.debug('city is empty')
      database.getcity(city,(err, result) => {
        
        if(err) {
          res.status(400).json({
            result: err
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
       })
        //name is undifined, city is correct
       }  
  
    //name and city are defined and in the data
    else {
      logger.debug(studenthome)
      database.getAllstudenthomes((err, result) => {
        if(err) {
          res.status(400).json({
            result: err
          })
        } else {
          res.status(200).json({
            result: result
          })
        }
      })
    }
  },

  // put UC-204, add studenthome

  deleteStudenthome(req, res, next) {
    logger.debug('Called delete on /api/studenthome')
    database.deleteStudenthome(req.params.homeId, req.body, (err, result) => {
      if (err) {
        logger.error('deleteStudenthome', error)
        res.status(400).json({
          result: err
        })
      } else {
        res.status(200).json({
          result: result.result
        })
      }
    })
  },


      // put UC-206, adding id from user and adding to a studenthouse.
      addIdToStudenthome(req, res, next)  {
        logger.info('Put is called on /api/studenthome/:homeId/user')
            
          
          },


}



module.exports = controller

