const config = require('../config/config')
const logger = config.logger
const assert = require('assert')
const database = require('../config/database')
const pool = require('../config/database')


let controller = {
    validateMeal(req, res, next) {
      try {
        const { name, description, ingredients, allergies,  createdon, offeredon, price, maxparticipants} = req.body
  
        assert(typeof name === 'string', 'name cannot be empty.')
        assert(typeof description === 'string', 'description cannot be empty.')
        assert(typeof ingredients === 'string', 'ingredients cannot be empty.')
        assert(typeof allergies === 'string', 'allergies cannot be empty.')
        assert(typeof createdon === 'string', 'createdon cannot be empty.')
        assert(typeof offeredon === 'string', 'offeredon cannot be empty.')
        assert(typeof price === 'number', 'price cannot be empty.')
        assert(typeof maxparticipants === 'number','max participants cannot be empty.')
        next()
      } catch (err) {
        res.status(400).json({
          message: 'Meal is not valid',
          error: err.toString()
        })
      }
    },


    createStudentMeal(req, res, next) {
      logger.info('Studentmeal is created at studenthome/:homeId/meal')

        database.createstudentMeal(req.body, req.params.homeId, (err, result) => {
      

          if(err){
            res.status(400).json({
              result: err
            })
          }else{
            res.status(200).json({
            result: result
            })
          }
        })
      },

      updateStudentmeal(req, res, next) {
        logger.info('studenthome is updated and called on /api/studenthome/')
        const homeId = req.params.homeId
        const mealid = req.params.mealid

      
          database.updateStudentMeals(req.body, homeId, mealid, (err, result) => {
    
            if(err) {
              res.status(400).json({
                result: err
              })
            } else {
              res.status(200).json({
                result: result
              })
            }
    
          })
      },

      deleteStudentmeal(req, res, next) {
        logger.debug('Called delete on /api/studenthome')
        database.deleteStudentMeal(req.params.homeId, req.params.mealid, (err, result) => {
          if (err) {
            logger.error('getStudentmeal', error)
            res.status(400).json({
              result: err
            })
          } else {
            res.status(200).json({
              result: "Deleted"
            })
          }
        })
      },

      getdetailOfStudentMeal(req, res, next)  {
        logger.info('Get is called on /api/studenthome/.....')
      
        const homeId = req.params.homeId
        const mealid = req.params.mealid
        logger.info('Get is called on /api/studenthome/', homeId)
      
        database.getDetailOfStudentMeal(homeId, mealid, (err, result) => {

          if(err) {
        res.status(400).json({
          result: err
        })
      } else{
        res.status(200).json({
          result: result
        })
      }
    })

   }, 

   listOfStudentMeals(req, res, next)  {
    logger.info('Get is called on /api/studenthome/.....')
  
    const homeId = req.params.homeId
    logger.info('Get is called on /api/studenthome/', homeId)
  
    database.getlistOfstudentmeals(homeId,  (err, result) => {

      if(err) {
    res.status(400).json({
      result: err
    })
  } else {
    res.status(200).json({
      result: result
    })
  }
})

}, 
        
    }



module.exports = controller