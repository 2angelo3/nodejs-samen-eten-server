const express = require('express')
const router = express.Router()
const participantscontroller = require('../controllers/participants.controller')
const authcontroller = require('../controllers/authentication.controller')

// User routers

router.post(
    '/studenthome/:homeId/meal/:mealId/signup',
    authcontroller.validateToken,
    participantscontroller.countParticipants,
    participantscontroller.signUpMeal
  
  )
  
  //UC-402 Afmelden voor maaltijd
  router.put(
    '/studenthome/:homeId/meal/:mealId/signoff',
    authcontroller.validateToken,
    participantscontroller.signOffMeal
  )
  
  //UC-403 Lijst van deelnemers opvragen
  router.get('/meal/:mealId/participants',
  authcontroller.validateToken,
  participantscontroller.getAllParticipants)
  
  // //UC-404 Details van deelnemer opvragen
  router.get(
    '/meal/:mealId/participants/:participantId',
    authcontroller.validateToken,
    participantscontroller.getDetailOfParticipant
  )

module.exports = router