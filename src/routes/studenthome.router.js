const express = require('express')
const router = express.Router()
const studenthomecontroller = require('../controllers/studenthomes.controller')
const authcontroller = require('../controllers/authentication.controller')

// studenthome routers

//UC-103, gets information about the student
router.get('/info', studenthomecontroller.getstudentinfo)
  
//UC-201, makes an studenthome
router.post('/studenthome', authcontroller.validateToken,
 studenthomecontroller.validateStudenthome, 
studenthomecontroller.createStudenthome)
  
//UC-202, gives a list of all studenthouses
router.get('/studenthome', studenthomecontroller.getListOfAllStudents)
    
//UC-203, gives details about studenthomes with an id
router.get('/studenthome/:homeId',  studenthomecontroller.getdetailOfStudenthomes)
  
//UC-204, add studenthome
router.put('/studenthome/:homeId', authcontroller.validateToken,
 studenthomecontroller.validateStudenthome,
studenthomecontroller.updateStudenthome,
)
    
//UC-205, deleting studenthome
router.delete('/studenthome/:homeId', authcontroller.validateToken,
studenthomecontroller.deleteStudenthome)
  
//UC-206, adding id from user and adding to a studenthouse.
//router.put('/studenthome/:homeId/user', studenthomecontroller.updateStudenthome)

module.exports = router