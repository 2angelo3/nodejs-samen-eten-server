const express = require('express')
const router = express.Router()
const studentmealcontroller = require('../controllers/meal.controller')
const authcontroller = require('../controllers/authentication.controller')


// UC-301 Maaltijd aanmaken
router.post('/studenthome/:homeId/meal', authcontroller.validateToken,
 studentmealcontroller.createStudentMeal
, studentmealcontroller.validateMeal)

//UC-302 Maaltijd wijzigen
router.put('/studenthome/:homeId/meal/:mealid', authcontroller.validateToken,
 studentmealcontroller.updateStudentmeal,
studentmealcontroller.validateMeal)

// UC-303 Lijst van maaltijden opvragen
router.get('/studenthome/:homeId/meal', studentmealcontroller.listOfStudentMeals)

//UC-304 Details van een maaltijd opvragen
router.get('/studenthome/:homeId/meal/:mealid', studentmealcontroller.getdetailOfStudentMeal)

//UC-305 Maaltijd verwijderen
router.delete('/studenthome/:homeId/meal/:mealid', authcontroller.validateToken,
 studentmealcontroller.deleteStudentmeal)

module.exports = router