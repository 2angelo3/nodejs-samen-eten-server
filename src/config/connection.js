
var mysql = require('mysql');
const dbconfig = require('./config').dbconfig

var connection = mysql.createConnection({
    host     : dbconfig.host,
    user     : dbconfig.user,
    password : dbconfig.password,
    database : dbconfig.database
});

connection.connect(function(err) {
    if (err) throw err;
});

module.exports = connection;
