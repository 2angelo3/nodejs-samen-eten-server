const mysql = require('mysql')
const connection = require('./connection')
const logger = require('../config/config').logger


let database = {

    createstudenthome(studenthome,callback) {
    
          const  {Name, Address, House_Nr, UserID, Postal_Code, Telephone, City } = studenthome

         // Use the connection
         connection.query(`INSERT into studenthome (Name, Address, House_Nr, UserID, Postal_Code, Telephone, City) 
         VALUES('${Name}', '${Address}', ${House_Nr}, ${UserID}, '${Postal_Code}', '${Telephone}', '${City}') `,
         function (error, results, fields) {
          if (error) {
            callback(undefined, {
              result: error
            })
          } else {
            callback(undefined, {
              ID: results.insertId,
              result: studenthome
            })
          }
        }
      )
    },

    updateStudenthomes(updatedHome, homeId,  callback){

      const  {Name, Address, House_Nr, UserID, Postal_Code, Telephone, City } = updatedHome

        // Use the connection 
        connection.query(`UPDATE studenthome SET name = '${Name}', address = '${Address}', house_nr = ${House_Nr}, userId = ${UserID}, postal_code = '${Postal_Code}', city = '${City}', telephone = '${Telephone}' WHERE ID = ${homeId} `,
        
        function (error, results, fields) {
          if (error) {
            callback(undefined, {
              result: error
            })
          } else if (results.affectedRows === 0) {
            callback(undefined, {
              result: 'Studenthome does not exist'
            })
          } else {
            callback(undefined, {
              result: updatedHome
            })
          }
        }
      )
    },


    
 

    getAllstudenthomes(callback) {

  connection.query('SELECT * FROM studenthome', function (error, results, fields) {
    if (error) throw error
  
    logger.debug('results: ', results)
    callback(undefined, {
        result: results
      })
      
    })
  
 },


 getStudenthome(id, callback) {

 connection.query(`SELECT * FROM studenthome WHERE id = ${id}`, 
 function (error, results, fields) {
 
   if (error) throw error
    logger.info(error)
    
  logger.debug('results: ', results)
  callback(undefined, {
    result: results
        })

    })
},

 
 
 getcity(city, callback) {
  
  connection.query(`SELECT * FROM studenthome WHERE City = "${city}"` , 
  function (error, results, fields) {
    if (error) throw error
  
    logger.debug('results: ', results)
    callback(undefined, {
        result: results
      })
   })
  
 },

 
 getName(name, callback) {

  connection.query(`SELECT * FROM studenthome WHERE name = "${name}"`, function (error, results, fields) {
    if (error) throw error
  
    logger.debug('results: ', results)
    callback(undefined, {
        result: results
      })
     
  })
  
 }, 

 getnameAndCity(name, city, callback) {
   connection.query(`SELECT * FROM studenthome WHERE Name= "${name}" AND City= "${city}"`, function(error, results, fields){
     if(error) throw error
     logger.debug('results: ', results)
     callback(undefined, {
       results: results

     })
   })

},



 
deleteStudenthome(homeId, body, callback){
    
      connection.query(`DELETE FROM studenthome where ID = ${homeId} `,
      function (error, results, fields) {
        if (error) {
          logger.debug('results: ', results)
          callback(undefined, {
            result: error
          })
        } else if (results.affectedRows === 0) {
          callback(undefined, {
            result: 'Studenthome does not exist'
          })
        } else {
          callback(undefined, {
            result: body
          })
        }
      }
    )
  },

createstudentMeal(studentMeal, homeid, callback) {
    
  const  {name, description, ingredients, allergies , createdon, offeredon, price, userid, studenthomeid,  maxparticipants } = studentMeal

      connection.query(`INSERT into meal (name, description, ingredients, allergies, createdon,  offeredon, price, userid, studenthomeid, maxparticipants) 
      VALUES ('${name}', '${description}' , '${ingredients}' , '${allergies}' , '${createdon}' , '${offeredon}' , ${price} , 1 , ${homeid},   ${maxparticipants} ) `,
        function (error, results, fields) {
      if (error) throw error

      logger.debug('results: ', results)
      callback(undefined, {
        ID: results.insertId,
      result: studentMeal
             })
          })
      },

 updateStudentMeals(updatedStudentMeal,homeid, mealid, callback){
   
      const {  name, description, ingredients, allergies , createdon, offeredon, price, userid, maxparticipants} = updatedStudentMeal       
     
      connection.query(`UPDATE meal SET Name = '${name}', Description = '${description}', Ingredients = '${ingredients}', Allergies = '${allergies}', CreatedOn = '${createdon}', OfferedOn = '${offeredon}', Price = ${price}, UserID =  ${userid}, MaxParticipants = ${maxparticipants} WHERE studenthomeID = ${homeid} AND ID =  ${mealid}`,
      function (error, results, fields) {
        if (error) throw error;
        logger.debug('The solution is: ', results);
        callback(undefined, {
            result: updatedStudentMeal
        })
        
      })
  
},

deleteStudentMeal(homeid, mealid, callback){
    
      connection.query(`DELETE IGNORE FROM meal WHERE ID = ${mealid} AND StudenthomeID = ${homeid}`,
      function (error, results, fields) {
        if (error) throw error;
        logger.debug('The solution is: ', results);
        callback(undefined, {
            result: "deletedmeal"
        })
    
      })

  },

  getDetailOfStudentMeal(homeId, mealid, callback) {

   connection.query(`SELECT * FROM meal WHERE studenthomeId = ${homeId} AND ID = ${mealid}`, 
   function (error, results, fields) {
    if (error) throw error;
    logger.debug('The solution is: ', results);
    callback(undefined, {
        result: results
         })
       
     })
   
  },

  getlistOfstudentmeals(homeid, callback) {

   connection.query(`SELECT * FROM meal WHERE studenthomeId = ${homeid}`, function (error, results, fields) {
     if (error) throw error
     logger.debug('results: ', results)
     callback(undefined, {
         result: results
       })
       
     })
   
  },

  verifyParticipants(mealId, callback) {
    connection.query(
      `SELECT count(mealid) AS amountparticipants from participants where mealid = '${mealId}'`,
      function (error, results, fields) {
        if (error) throw error
        let amountParticipants = results[0].amountparticipants
        connection.query(
          `SELECT maxparticipants from meal where id = '${mealId}'`,
          function (error, partResult) {
            if (error) throw error
            else {
              let maxParticipants = partResult[0].maxparticipants
              console.log(amountParticipants < maxParticipants)
              if (amountParticipants < maxParticipants) {
                callback(undefined, {
                  result: 'There is space available'
                })
              } else {
                callback(undefined, {
                  result: "There is no more space available"
                })
              }
            }
          }
        )
      }
    )
  },

  signUpMeal(userId, homeId, mealId, callback) {
    connection.query(
      `INSERT INTO participants (userId, StudenthomeID, MealID, SignedUpOn) VALUES ('${userId}', '${homeId}', '${mealId}', "2010-01-01")`,
      function (error, results, fields) {
        if (error) {
          callback(undefined, {
            result: error
          })
        } else {
          callback(undefined, {
            result: results
          })
        }
      }
    )
  },
signOffMeal (userId, homeId, mealId, callback) {
  connection.query(`DELETE FROM participants where userId = '${userId}' AND mealId = '${mealId}' AND studenthomeId = '${homeId}' `,
    function (error, results, fields) {
    if (error) {
      callback(undefined, {
        result: error
      })
    } else {
      callback(undefined, {
        result: results
      })
    }
  }
)
},

getAllParticipants(mealId,callback) {
  connection.query(`SELECT * FROM participants where mealId = '${mealId}' `,
    function (error, results, fields) {
    if (error) {
      callback(undefined, {
        result: error
      })
    } else {
      callback(undefined, {
        result: results
      })
    }
  }
)
},
getDetailOfParticipant(userId,mealId,callback) {
  connection.query(`SELECT user.id, user.first_name, user.last_name,
   user.email, user.student_number
    FROM user JOIN participants ON participants.userid = user.id 
    JOIN meal ON meal.id = participants.mealid WHERE user.id = '${userId}' 
    AND meal.id = '${mealId}' `, function (error, results, fields) {
      if (error) {
        callback(undefined, {
          result: error
        })
      } else {
        callback(undefined, {
          result: results
        })
      }
    }
  )
}


}


module.exports = database