require('dotenv').config()
const loglevel = process.env.LOGLEVEL || 'debug'

module.exports = {
  dbconfig: {
    host: process.env.DB_HOST || '188.166.109.108',
    user: process.env.DB_USER || 'Angelo_van_Peer',
    database: process.env.DB_DATABASE || '2139170',
    password: process.env.DB_PASSWORD ,
    connectionLimit: 10
  },

  logger: require('tracer').console({
    format: ['{{timestamp}} [{{title}}] {{file}}:{{line}} : {{message}}'],
    preprocess: function (data) {
      data.title = data.title.toUpperCase()
    },
    dateformat: 'isoUtcDateTime',
    level: loglevel
  })
}
